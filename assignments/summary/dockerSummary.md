<h1>Docker Basics</h1>

<h3>Why Docker?</h3>

Developing apps today require so much more than code - Multiple languages, frameworks, architectures, and discontinuous interfaces between tools. 

Docker simplifies and accelerates your workflow, while giving developers the freedom to innovate with their choice of tools, application stacks, and deployment environments for each project.

*In a nutshell, Docker lets you `handle dependencies` as the complexity of the project increases.*

<h3>What is Docker?</h3>

Docker is an open-source project for automating the deployment of applications as portable, self-sufficient containers that can run on the cloud or on-premises. 

<h3>Important Terminology</h3>

<h4>Docker Image</h4>

A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application:
 - code 
 - runtime
 - system tools
 - system libraries
 - settings
 - configuration files
 - environment variables

This image is then deployed using the `Docker Engine` as the `container`.

<h4>Container</h4>

Running Docker image.

Multiple containers can be created from one image.

![Docker Containers](/extras/DockerContainers.png)
<h4>Docker Hub</h4>

Like GitHub, but for Docker images and containers.

![Docker Hub](/extras/DockerHub.png)

### A simple analogy
Perhaps a simple analogy can help getting the grasp of the core concept of Docker.

Let's go back in time to the 1950s for a moment. There were no word processors, and the photocopiers were used everywhere!

Imagine you're responsible for quickly issuing batches of letters as required, to mail them to customers, using real paper and envelopes, to be delivered physically to each customer's address (there was no email back then).

At some point, you realize the letters are just a composition of a large set of paragraphs, which are picked and arranged as needed, according to the purpose of the letter, so you devise a system to issue letters quickly, expecting to get a hefty raise.

The system is simple:

You begin with a deck of transparent sheets containing one paragraph each.

To issue a set of letters, you pick the sheets with the paragraphs you need, then you stack and align them so they look and read fine.

Finally, you place the set in the photocopier and press start to produce as many letters as required.

So, simplifying, that's the core idea of Docker.

*In Docker, each layer is the resulting set of changes that happen to the filesystem after executing a command, such as, installing a program.*

So, when you "look" at the filesystem after the layer has been copied, you see all the files, included the layer when the program was installed.

You can think of an image as an auxiliary read-only hard disk ready to be installed in a "computer" where the operating system is already installed.

Similarly, you can think of a container as the "computer" with the image hard disk installed. The container, just like a computer, can be powered on or off.

<h3>Installation</h3>

Follow the link provided for a step-by-step guide to install Docker:

[Install Docker](https://docs.docker.com/engine/install/ubuntu/)

<h3>Docker basic commands</h3>

**docker ps** : Allows us to view all the containers that are running on the Docker Host.

**docker start** : Starts any stopped container(s).

**docker stop** : Stops any running container(s).

**docker run** : reates containers from docker images.

**docker rm** : Deletes the container.

<h3>Working with Docker</h3>

 1. Download the docker image.
```bash
docker pull <image-author/image-name>/trydock
```
 2. Run the docker image with this command.
```bash
docker run -ti <image-author/image-name>/trydock /bin/bash
```
Every running container has an ID in your system.

 3. Let's go a little deeper 
 
Copy our code inside docker with this command. (Make sure you are not inside docker terminal, enter exit in command line to get out of container terminal)

In this case, lets copy a simple python program which just prints "hello, I am talking from container". So lets consider you have this file `hello.py`

```bash
print("hello, I am talking from container")
```
Copy file inside the docker container
```bash
docker cp hello.py <containerID>:/
```
Write a script for installing dependencies - requirements.sh
```bash
apt update
apt install python3
```
Copy file inside docker
```bash
docker cp requirements.sh <containerID>:/
```
 4. Install dependencies
If you want to install additional dependencies, write all the steps for installation in a shell script and run the script inside the container.
```bash
//Give permission to run shell script

docker exec -it <containerID> chmod +x requirements.sh

//Install dependencies

docker exec -it <containerID> /bin/bash ./requirements.sh
```

 5. Run the program inside container with this command.
 ```bash
docker start <containerID>
docker exec <containerID> python3 hello.py
```
 6. Save your copied program inside docker image with docker commit.
 ```bash
docker commit <containerID> <image-author/image-name>/trydock
```
 7. Push docker image to the dockerhub.

Tag image name with a different name

```bash
docker tag <image-author/image-name>/trydock <username>/<name-of-Image>
```
 8. Push to dockerhub

```bash
docker push <username>/<name-of-Image>
```






