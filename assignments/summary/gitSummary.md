<h2>Git Basics<h2>

<h4>What is Git?<h4>

Git is a `free and open source distributed version control system`.

It lets you `collaborate` on projects/research with others. 

Git allows to easily `keep track of the revisions` made by you and your teammates.

<h4>How does Git work?<h4>

The major difference between Git and other VCS is the way Git handles its data. Conceptually most other systems such as CVS, Bazaar, Subversion, Perforce, etc., stores information as a list of file-based changes, i.e., `delta-based version control`

Git treats data as a `stream of snapshots` of a miniature filesystem.
 - Takes a picture of what all the files look like at a particular time
 - Stores a reference to that snapshot
 - If some files have not changed, a link to the previous identical file is retained

<h4>Features of Git<h4>
<h5>Nearly every operation is local<h5>

Most operations in Git `need only local files and resources to operate` — generally no information is needed from another computer on your network
That means, you can `make commits to your local copy`, even without a network connection to upload
<h5>Integrity<h5>

Everything in Git is `checksummed` before it is stored and is then referred to by that checksum. 

This functionality is built into Git at its lowest levels i.e., you can’t lose information in transit or get file corruption without Git being able to detect it.

The mechanism used for this checksumming is called a `SHA-1 hash`. This is a 40-character string composed of hexadecimal characters (0–9 and a–f) and calculated based on the contents of a file or directory structure in Git. 

A SHA-1 hash looks something like this:
**da39a3ee5e6b4b0d3255bfef95601890afd80709**

Git stores everything in its database not by file name but by the hash value of its contents.
<h5>Only adds data<h5>

Nearly all actions in Git only adds data to the database. 
 
It is hard to get the system to do anything that is irreversible or to make it erase data in any way. 

As with any VCS, you can lose or mess up changes you haven’t committed yet, but after you commit a snapshot into Git, it is very difficult to lose, especially if you regularly push your database to another repository.

This makes using Git a joy because we know we can experiment without the danger of severely screwing things up.
<h5>Distributed development<h5>

![Distributed Development](/extras/DistributedDevelopment.jpg)
<h5>Git Workflow<h5>

![Workflow](/extras/Workflow.jpg)
The Git workflow is divided into three states:

 - Working directory - Modify files in your working directory
 - Staging area (Index) - Stage the files and add snapshots of them to your staging area
 - Git directory (Repository) - Perform a commit that stores the snapshots permanently to your Git directory. Checkout any    existing version, make changes, stage them and commit.

![States](/extras/States.jpg)
<h4>Important Terminology<h4>
<h5>Repository(-repo)<h5>

Repository is a collection of files and folders that you’re using git to track.

<h5>GitLab<h5>

The 2nd most popular remote storage solution for git repos. 1st being GitHub.
<h5>Commit<h5>

Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will `only exist on your local machine` until it is pushed to a remote repository.
<h5>Push<h5>

Pushing is essentially syncing your commits to the repository.

![Commits](/extras/Commits.png)
<h5>Branch<h5>

You can think of your git repo as a tree. The trunk of the tree, the main software, is called the `Master Branch`. The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.
<h5>Merge<h5>

When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it can be merged into the master branch. Merging is just what it sounds like: `integrating two branches together`.

![Branches and Merging](/extras/Branching.jpg)
<h5>Clone<h5>

Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and `makes an exact copy` of it on your local machine.

![Clone](/extras/Clone.png)
<h5>Fork<h5>

Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

![Fork](/extras/Fork.png)
<h4> Getting started<h4>
<h5> How to install Git<h5>

For Linux, open the terminal and type
```bash
    sudo apt-get install git
```
Clone the repo

```bash
    $ git clone <link-to-repository> 
```
Create a new branch

```bash
    $ git checkout master
    $ git checkout -b <your-branch-name>
```
You modify files in your working tree.

You selectively stage just those changes you want to be part of your next commit,
which adds only those changes to the staging area.
```bash
    $ git add .         # To add untracked files ( . adds all files) 
```
You do a commit, which takes the files as they are in the staging area and stores that
snapshot permanently to your Local Git Repository.
```bash
    $ git commit -sv   # Description about the commit
```
You do a push, which takes the files as they are in the Local Git Repository and stores
that snapshot permanently to your Remote Git Repository.
```bash
    $ git push origin <branch-name>      # push changes into repository
```
<h4>Summary<h4>
![Implementation](/extras/Implementation.png)